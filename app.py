from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Define enumerate as a global variable to be used in the template
app.jinja_env.globals['enumerate'] = enumerate

class TodoList:
    def __init__(self):
        self.tasks = []

    def add_task(self, task):
        self.tasks.append({"task": task, "completed": False})

    def view_tasks(self):
        return self.tasks

    def mark_completed(self, index):
        if 0 <= index < len(self.tasks):
            self.tasks[index]["completed"] = True

    def delete_task(self, index):
        if 0 <= index < len(self.tasks):
            del self.tasks[index]

todo_list = TodoList()

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        task = request.form['task']
        todo_list.add_task(task)
    return render_template('index.html', tasks=todo_list.view_tasks())

@app.route('/complete/<int:index>')
def complete(index):
    todo_list.mark_completed(index)
    return redirect(url_for('index'))

@app.route('/delete/<int:index>')
def delete(index):
    todo_list.delete_task(index)
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

