import unittest
from app import app, TodoList

class TestTodoList(unittest.TestCase):
    def setUp(self):
        self.app = app.test_client()
        self.todo_list = TodoList()

    def test_index_route(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)

    def test_add_task(self):
        with app.test_request_context('/'):
            self.todo_list.add_task('Test Task')
            tasks = self.todo_list.view_tasks()
            self.assertEqual(len(tasks), 1)
            self.assertEqual(tasks[0]['task'], 'Test Task')
            self.assertFalse(tasks[0]['completed'])

    def test_mark_completed(self):
        with app.test_request_context('/'):
            self.todo_list.add_task('Test Task')
            self.todo_list.mark_completed(0)
            tasks = self.todo_list.view_tasks()
            self.assertTrue(tasks[0]['completed'])

    def test_delete_task(self):
        with app.test_request_context('/'):
            self.todo_list.add_task('Test Task')
            self.todo_list.delete_task(0)
            tasks = self.todo_list.view_tasks()
            self.assertEqual(len(tasks), 0)

if __name__ == '__main__':
    unittest.main()

